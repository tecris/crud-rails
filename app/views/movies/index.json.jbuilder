json.array!(@movies) do |movie|
  json.extract! movie, :id, :title, :year, :country
  json.url movie_url(movie, format: :json)
end
