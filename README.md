# README

#### Ruby version

#### System dependencies

#### Configuration

#### Database creation

#### Database initialization

#### How to run the test suite

#### Services (job queues, cache servers, search engines, etc.)

#### Deployment instructions

#### Run demo (with docker compose)

   ```
   $ docker build --no-cache -t bluesky/volga-web:1.0.0 .    # build web image
   $ docker-compose -f compose-demo.yml up -d                # start db and web containers
   $ rake db:migrate                                         # apply database schema
   $ rake db:seed                                            # add sample data
   $ curl -i -H "Accept: application/json"  http://localhost:3000/movies
   ```

#### Development

  ```
   $ git clone git@github.com:tecris/crud-rails.git
   $ cd crud-rails
   $ bundle install
   $ docker-compose up -d
   $ rake db:migrate
   $ rake db:seed
   $ rails server
   $ curl -i -H "Accept: application/json"  http://localhost:3000/movies
  ```

#### REST api
| Verb | URI Pattern |
|------|-------------|
| GET    | /movies |
| POST   | /movies | 
| GET    | /movies/:id | 
| DELETE | /movies/:id | 

 - **Examples**
  - Create

    ```
    $ curl -i -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '{
      "title": "Cloud Atlas",
      "country": "Germany",
      "year": "2011"
    }' http://localhost:3000/movies
    ```
    
  - Read (all)

    `$ curl -i -H "Accept: application/json"  http://localhost:3000/movies`
    
  - Read (one)

    `$ curl -i -H "Accept: application/json"  http://localhost:3000/movies/1`
    
  - Update

    ```
    $ curl -i -H "Content-Type: application/json" -H "Accept: application/json" -X PUT -d '{
      application/json" -X PUT -d '{
      "title": "Cloud Atlas",
      "country": "Germany",
      "year": "2039"
    }' http://localhost:3000/movies/1
    ```
  - Delete

    `$ curl -i -H "Accept: application/json" -X DELETE http://localhost:3000/movies/1`
