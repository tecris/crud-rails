################################################################################
#
# .) Build
# docker build --no-cache -t bluesky/volga-web:1.0.0 .
#
################################################################################

FROM rails:4.2.4

RUN mkdir /volga
WORKDIR /volga
ADD Gemfile /volga/Gemfile
ADD Gemfile.lock /volga/Gemfile.lock
RUN bundle install
ADD . /volga

CMD ["/volga/bin/rails", "server", "--port", "3000", "--binding", "0.0.0.0"]
