# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


movie_list = [
  [ "Cloud Atlas", "Germany", 2012 ],
  [ "BD in Actiune", "Romania", 1970 ],
  [ "Fantomas", "France", 1964 ]
]

movie_list.each do |title, country, year|
  Movie.create(title: title, country: country, year: year)
end
